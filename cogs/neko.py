import nekos
import urllib



import discord
from discord import app_commands
from discord.ext import commands
from discord.ext.commands import Context


# from nekos import img
from nekos import *

from helpers import checks, db_manager


class Neko(commands.Cog, name="neko"):
    def __init__(self, bot):
        self.bot = bot
    def img(target: str):
        target ==  [
            "wallpaper",
            "ngif",
            "tickle",
            #"lewd", Provides 1 image
            "feed",
            "gecg",
            "gasm",
            "slap",
            "avatar",
            "lizard",
            "waifu",
            "pat",
            "8ball",
            "kiss",
            "neko",
            "spank",
            "cuddle",
            "fox_girl",
            "hug",
            "smug",
            "goose",
            "woof",
        ]
#-------------------------------------------------------------------------------

    @commands.hybrid_group(
        name="nekos",
        description="All nekos commands.",
    )
    @checks.not_blacklisted()
    async def nekos(self, context: Context) -> None:

        if context.invoked_subcommand is None:
            embed = discord.Embed(
                title="All targets for nekos api",
                description="Please specify a target.\n\n`wallpaper`, `ngif`, `tickle`, `feed`, `gecg`, `gasm`, `slap`, `avatar`, `lizard`, `waifu`, `pat`, `8ball`, `kiss`, `neko`, `spank`, `cuddle`, `fox_girl`, `hug`, `smug`, `goose`, `woof`",
                color=0xE02B2B
            )
            await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        base="nekos",
        name="wallpaper",
        description="Send a Random Anime image in channel.(NSFW included)",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def wallpaper(self, context: commands.Context, target: str = "Wallpaper") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos Wallpaper",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="ngif",
        description="Ramdom ngif.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def ngif(self, context: commands.Context, target: str = "ngif") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos Wallpaper",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="tickle",
        description="Ramdom tickle.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    @app_commands.describe(user="The user that that you want to tickle")
    async def tickle(self, context: commands.Context, user: discord.User, target: str = "tickle") -> None:

        url = (nekos.img(target))
        data = (user.mention)
        embed = discord.Embed(
            title="Nekos tickle",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(data)
        await context.send(embed=embed)
#-------------------------------------------------------------------------------

    @nekos.command(
        name="feed",
        description="Ramdom feed.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def feed(self, context: commands.Context, target: str = "feed") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos feed",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="gecg",
        description="Ramdom gecg.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def gecg(self, context: commands.Context, target: str = "gecg") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos gecg",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="gasm",
        description="Ramdom gasm.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def gasm(self, context: commands.Context, target: str = "gasm") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos gasm",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="slap",
        description="Ramdom Anime slap.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def slap(self, context: commands.Context, target: str = "slap") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos slap",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="avatar",
        description="Ramdom avatar.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def avatar(self, context: commands.Context, target: str = "avatar") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos avatar",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="lizard",
        description="Ramdom lizard.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def lizard(self, context: commands.Context, target: str = "lizard") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos lizard",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="waifu",
        description="Ramdom Anime waifu.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def waifu(self, context: commands.Context, target: str = "waifu") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos waifu",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="pat",
        description="Ramdom pat.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def pat(self, context: commands.Context, target: str = "pat") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos pat",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="eightball",
        description="Ramdom eightball.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def eightball(self, context: commands.Context, target: str = "8ball") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos eightball",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="neko",
        description="Ramdom neko.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def neko(self, context: commands.Context, target: str = "neko") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos neko",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="kiss",
        description="Ramdom kiss.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def kiss(self, context: commands.Context, target: str = "kiss") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos kiss",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="spank",
        description="Ramdom spank.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def spank(self, context: commands.Context, target: str = "spank") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos spank",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="cuddle",
        description="Ramdom cuddle.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def cuddle(self, context: commands.Context, target: str = "cuddle") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos cuddle",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="fox_girl",
        description="Ramdom fox_girl.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def fox_girl(self, context: commands.Context, target: str = "fox_girl") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos fox_girl",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="hug",
        description="Ramdom hug.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def hug(self, context: commands.Context, target: str = "hug") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos hug",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="smug",
        description="Ramdom smug.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def smug(self, context: commands.Context, target: str = "smug") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos smug",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="goose",
        description="Ramdom goose.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def goose(self, context: commands.Context, target: str = "goose") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos goose",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @nekos.command(
        name="woof",
        description="Ramdom woof.",
    )
    @checks.not_blacklisted()
    @app_commands.describe(target="This can be left blank")
    async def woof(self, context: commands.Context, target: str = "woof") -> None:

        url = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos woof",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------



    @commands.hybrid_command(
        name="cat",
        description="Send a random cat image",
    )
    @checks.not_blacklisted()
    async def cat(self, context: Context) -> None:
        url = (nekos.cat())
        embed = discord.Embed(
            title="Nekos Cat",
            description=f"{context.author}",
            color=0x9C84EF
        )
        embed.set_image(
            url=url
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="name",
        description="Ramdom Name Text.",
    )
    @checks.not_blacklisted()
    async def name(self, context: Context) -> None:
        description = (nekos.name())
        embed = discord.Embed(
            title="Nekos Cat",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="why",
        description="Why.",
    )
    @checks.not_blacklisted()
    async def why(self, context: Context) -> None:
        description = (nekos.why())
        embed = discord.Embed(
            title="Nekos Why",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="nfact",
        description="Returns a random Fact.",
    )
    @checks.not_blacklisted()
    async def nfact(self, context: Context) -> None:
        description = (nekos.fact())
        embed = discord.Embed(
            title="Nekos fact",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="textcat",
        description="Ramdom Name Textcat.",
    )
    @checks.not_blacklisted()
    async def textcat(self, context: Context) -> None:
        description = (nekos.textcat())
        embed = discord.Embed(
            title="Nekos textcat",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="owoify",
        description="Ramdom Name Textcat.",
    )
    @checks.not_blacklisted()
    async def owoify(self, context: Context, text: str) -> None:
        description = (nekos.owoify(text))
        embed = discord.Embed(
            title="Nekos owoify",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="lewd",
        description="Ramdom lewd.",
    )
    @checks.not_blacklisted()
    async def lewd(self, context: Context, target: str = 'lewd') -> None:
        description = (nekos.img(target))
        embed = discord.Embed(
            title="Nekos lewd",
            description=description,
            color=0x9C84EF
        )
        await context.send(embed=embed)

#-------------------------------------------------------------------------------


async def setup(bot):
    await bot.add_cog(Neko(bot))
