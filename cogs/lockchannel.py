import discord

from discord.ext import commands
from discord.ext.commands import Context

from helpers import checks


class Lockchannel(commands.Cog, name="lockchannel"):
    def __init__(self, bot):
        self.bot = bot

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="lockchannel",
        description="Lock Server Channel.",
    )
    @commands.has_permissions(manage_channels=True)
    @checks.not_blacklisted()
    @checks.is_owner()
    async def lock(self, context, channel: discord.TextChannel = None, reason=None) -> None:
        if reason == None:
            reason = 'Channel Locked By Owner'
        channel = context.channel or channel
        await channel.set_permissions(context.guild.default_role, send_messages=False, add_reactions=False, create_public_threads=False)
        await context.send(f"`{context.author}` - `{context.author.id}` has locked <#{channel.id}>\nReason: `{reason}`")

#-------------------------------------------------------------------------------

    @commands.hybrid_command(
        name="unlockchannel",
        description="Unlock Server Channel.",
    )
    @commands.has_permissions(manage_channels=True)
    @checks.not_blacklisted()
    @checks.is_owner()
    async def unlock(self, context, channel: discord.TextChannel = None, reason=None) -> None:
        if reason == None:
            reason = 'Channel Locked By Owner'
        channel = context.channel
        await channel.set_permissions(context.guild.default_role, send_messages=None, add_reactions=None, create_public_threads=None)
        await context.send(f"`{context.author}` - `{context.author.id}` has unlocked <#{channel.id}>\nReason: `{reason}`")

#-------------------------------------------------------------------------------


async def setup(bot):
    await bot.add_cog(Lockchannel(bot))
