import discord
from discord import app_commands
from discord.ext import commands
from discord.ext.commands import Context

from helpers import checks


# Here we name the cog and create a new class for the cog.
class Infomantion(commands.Cog, name="infomantion"):
    def __init__(self, bot):
        self.bot = bot

    # Here you can just add your own commands, you'll always need to provide "self" as first parameter.


    @commands.hybrid_command(
        name='links',
        description="This is a testing command that does nothing.",
        aliases=['link']
    )
    @checks.not_blacklisted()
    async def links(self, context):
        """Lists all my important links in an embed."""
        links = discord.Embed(
            title="List of Links",
            description="Here is a list of links. More will be added later. Enjoy!",
            colour=0xFF0000
        )
        links.set_author(
            name="Die-Antwoord"
        )
        links.add_field(
            name="Aero#5703 - 432129282710700033",
            value="https://aero.bot/",
            inline=False
        )
        links.add_field(
            name="Reputation#1740 - 550035183269838848",
            value="https://discordrep.com/",
            inline=False
        )
        links.add_field(
            name="Blacklister#3409 - 866364881917837312",
            value="https://discord.gg/FZxgNrwBS6",
            inline=False
        )
        links.add_field(
            name="DangerCord - 1007806961854726269",
            value="▌<@1007806961854726269> | https://dangercord.com/",
            inline=False
        )
        links.add_field(
            name="GitHub",
            value="https://github.com/Die-Antwoord",
            inline=False
        )
        await context.send(embed=links)
        pass


# And then we finally add the cog to the bot so that it can load, unload, reload and use it's content.
async def setup(bot):
    await bot.add_cog(Infomantion(bot))