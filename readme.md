# Discord-Bot-Python

- Python and the library resources [Here](https://pythondiscord.com/pages/resources)
- Clone/Download the repository - ``git clone`` (RUN in Command-Promt) [Windows key + r , input ``cmd`` and press enter]

```shell
$ git clone https://github.com/Die-Antwoord/Discord-Bot-Python.git
$ cd ./Discord-Bot-Python
```

## Create Bot

- Create a Discord Bot: [HERE](https://discord.com/developers/applications)
- Permission Generator: [HERE](https://discordapi.com/permissions.html)
- Invite Your Bot: <https://discord.com/oauth2/authorize?&client_id=YOUR_APPLICATION_ID_HERE&scope=bot+applications.commands&permissions=8>


## Create .env enviroment

- NOTE: If you going to use .env environment you will need to make the necessary changes to the bots code inorder for it to work
- Using .env files can be found [here](https://dev.to/jakewitcher/using-env-files-for-environment-variables-in-python-applications-55a1
)
- Create [.env](.env) file in the root director.
- Here is an explanation of what the values should look like: [env_demo.env](env_demo.env)

[env_demo.env](env_demo.env)
```env
TOKEN=YOUR_BOT_TOKEN_HERE
WEBHOOK=YOUR_WEBHOOK_HERE
TOMORROWIO_TOKEN=WEATHER_API_TOKEN
```

## Create config.json


- Create [config.json](config.json) file in the root directory.
- Here is an explanation of what the values should look like: [demo.config.json](demo.config.json)
- Example:

[demo.config.json](demo.config.json)
```json
{
  "prefix": "YOUR_BOT_PREFIX_HERE", // '.' or '!' Whatever you specify
  "token": "YOUR_BOT_TOKEN_HERE", // Bot Token here
  "permissions": "YOUR_BOT_PERMISSIONS_HERE", // Example '8' or '1024' https://discordapi.com/permissions.html
  "application_id": "YOUR_APPLICATION_ID_HERE ", // Bot ID
  "sync_commands_globally": true,
  "owners": [
    YOUR_OWNERS_ID_HERE,
    YOUR_OWNERS_ID_HERE
  ], // Owners ID here 'can be more than one separated with ','
  "TOMORROWIO_TOKEN": "WEATHER_API_TOKEN", //http://api.timezonedb.com/v2.1/get-time-zone
  "allowguilds": "GUILD_ID_ALLOWED", //GUILD Allowed to use the bot
  "guildmsgdisable": "GUILD_ID_DISALLOW_MESSAGES", //GUILDS to where the bot should not respond to
  "roles": "JOIN_ROLE_ID", //ROLES that member gain at GUILD_JOIN
  "welcome_mention": "ROLE_ID_ON_USER_JOIN" // ROLE_ID that will be mentioned as a member join the server
}
```

## Dependences

- [PYTHON](https://www.python.org/downloads/)

## Install BOT Requirements

- RUN in Command-Promt [Windows key + r , input ``cmd`` and press enter]

```choc
$ pip install -r requirements.txt
```

## Run BOT

- If you have just installed python today, then you just need to use the following command:

```shell
$ python main.py
```
- If you have multiple versions of python installed (2.x and 3.x) then you will need to use the following command:

```shell
$ python3 main.py
```
- or eventually

```shell
$ python3.x main.py
```
- Replace `x` with the version of Python you have installed.

