# Linux Guide

------------------------------------

## Step 1 — Update and Upgrade

Logged into your Ubuntu 20.04 server as a sudo non-root user, first update and upgrade your system to ensure that your shipped version of Python 3 is up-to-date.

```
sudo apt update
sudo apt -y upgrade
```

- Confirm installation if prompted to do so.

## Step 2 — Check Version of Python

Check which version of Python 3 is installed by typing:

```
python3 -V
```

- You’ll receive output similar to the following, depending on when you have updated your system.

Output

```
Python 3.8.2
```

## Step 3 — Install pip

To manage software packages for Python, install pip, a tool that will help you manage libraries or modules to use in your projects.

```
sudo apt install -y python3-pip
```

- Python packages can be installed by typing:

```
pip3 install package_name
```

Here, package_name can refer to any Python package or library, such as Django for web development or NumPy for scientific computing. So if you would like to install NumPy, you can do so with the command 

```
pip3 install numpy.
```

## Step 4 — Install Additional Tools

There are a few more packages and development tools to install to ensure that we have a robust set-up for our programming environment:

```
sudo apt install build-essential libssl-dev libffi-dev python3-dev
```

## Step 5 — Install venv

Virtual environments enable you to have an isolated space on your server for Python projects. We’ll use venv, part of the standard Python 3 library, which we can install by typing:

```
sudo apt install -y python3-venv
```

## Step 6 — Create a Virtual Environment

You can create a new environment with the pyvenv command. Here, we’ll call our new environment my_env, but you should call yours something meaningful to your project.

```
python3 -m venv my_env
```

## Step 7 — Activate Virtual Environment

Activate the environment using the command below, where my_env is the name of your programming environment.

```
source my_env/bin/activate
```

- Your command prompt will now be prefixed with the name of your environment:

  
## Step 8 — Test Virtual Environment

Open the Python interpreter:

```
python
```

Note that within the Python 3 virtual environment, you can use the command python instead of python3, and pip instead of pip3.

- You’ll know you’re in the interpreter when you receive the following output:

```
Python 3.8.2 (default, Mar 13 2020, 10:14:16) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

- Now, use the print() function to create the traditional Hello, World program:

```
print("Hello, World!")
```

- Output

```
Hello, World!
```

## Step 9 — Deactivate Virtual Environment

Quit the Python interpreter:

```
quit()
```

Then exit the virtual environment:

```
deactivate
```

------------------------------------

## Creating a Python Virtual Environment for Your Project

Before you get started coding, you need to set up your Python developer environment. In this step, you will install and activate your Python requirements within a virtual environment for easier management.

- First, create a directory in your home directory that you can use to store all of your virtual environments:

```
mkdir ~/.venvs
```

- Now create your virtual environment using Python:

```
python3 -m venv ~/.venvs/discord
```

This will create a directory called discord within your .venvs directory. Inside, it will install a local version of Python and a local version of pip. You can use this to install and configure an isolated Python environment for your project.

- Before you install your project’s Python requirements, activate the virtual environment:

```
source ~/.venvs/discord/bin/activate
```

Your prompt should change to indicate that you are now operating within a Python virtual environment. It will look something like this: (discord)user@host:~$.

- With your virtual environment active, install discord.py with the local instance of pip:

```
pip install discord.py
```

Note: Once you have activate your virtual environment (when your prompt has (discord) preceding it), use pip instead of pip3, even if you are using Python 3. The virtual environment’s copy of the tool is always named pip, regardless of the Python version.

Now that you have the Discord package installed, you will need to save this requirement and its dependencies. This is good practice so you can recreate your developer environment as needed.

- Use pip to save your environment’s information to a requirements.txt file:

```
pip freeze > requirements.txt
```

------------------------------------

You now have the libraries necessary to build a discord bot.

## Deploying to Ubuntu 20.04

You can leave your bot running from your local machine, but having to leave a terminal active or worry about power outages can be annoying. For convenience, you can deploy it to a server.

It is best practice to run your bot as a non-root user. This tutorial uses the username sammy.

- First, copy your code and Python library requirements to the server using the scp command. Send everything to your user’s root directory:

```
scp bot.py requirements.txt sammy@your_server_ip:~
```

- Next, ssh into your server:

```
ssh sammy@my_server_ip
```

You will now enter commands into the server’s terminal.

You need to install the proper Python system packages to run your bot on your server.

Install python3, python3-venv, and screen:

```
sudo apt update && sudo apt install python3 python3-venv screen
```

Now, you’re going to use a tool called screen to create a virtual terminal. Without this tool, if you were to exit your terminal while the bot was running, the process would terminate, and your bot would go offline. With this tool, you can connect and disconnect to sessions so your code remains running. To learn more about screen, check out our tutorial on installing and using screen.

To start a screen session, use the following command:

```
screen
```

screen will prompt you with a license agreement. Press Return to continue.

Now that you have a virtual session set up, you need to create a Python virtual environment to run your code.

First, like before, create a directory to store your virtual environments:

```
mkdir ~/.venvs
```

Then create a new virtual environment:

```
python3 -m venv ~/.venvs/discord
```

Activate the virtual environment:

```
source ~/.venvs/discord/bin/activate
```

Next, install the necessary libraries using pip:

```
pip install -r requirements.txt
```

Before you can run your bot, you’ll need to export the DISCORD_TOKEN and DISCORD_GUILD so your bot can access the API Key and Guild information:

```
export DISCORD_TOKEN=YOUR_DISCORD_TOKEN
export DISCORD_GUILD=YOUR_DISCORD_GUILD
```

Finally, run your bot:

```
python3 bot.py
```

Your bot will start up and start accepting messages. You can disconnect from the screen session using the key combination CTRL + A + D. When you are ready to reconnect to the session, you can use this screen command:

```
screen -r
```

You have successfully built a Discord bot using Python. The application runs on a server and responds to certain phrases shared in a Guild.

------------------------------------------------